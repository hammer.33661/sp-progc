/*
    
    Function Optimisation Using a Genetic Algorithm
    Seminar work of `Programming in C' (year 2018)
    
    Author: Milan Kladivko
    
    
    Header file  `io.h': 
    For details see module file  `io.c'
    
    File contains functions taking care of reading and writing 
    all files required in this program. 
    
*/


#ifndef _IO_H
#define _IO_H

#include <stdio.h>
#include "genetics.h"

/** 
    Reads the external function executable located on first line
    and all the parameters located in the file.
*/
int process_metafile(char *metafile);


typedef struct Variable_ {
    unsigned line;
    double min;
    double max;
    char type;
    char *name;
} Variable;

Variable * new_variable(
    unsigned line, 
    char *min, char *max, char type, 
    char *name);
void destroy_variable(Variable ** var);
char * tostr_variable(Variable * var);

/* Forward reference */
typedef struct Member_ Member;

double fitness_of(Member *member);

void write_member_summary(Member *);
void write_generation_summary();

#endif 
