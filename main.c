/*
    
    Function Optimisation Using a Genetic Algorithm
    Seminar work of `Programming in C' (year 2018)
    
    Author: Milan Kladivko
    
    
    Entry point to the program. 
*/


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "globals.h"
#include "io.h"
#include "genetics.h"

/** Name of the generation summary output file */
#define GEN_FILENAME "gen.txt"
/** Name of the values output file */
#define VAL_FILENAME "val.txt"
/** Maximum number of members in every generation */
#define POPULATION_COUNT 100
/** Default mutation percentage to use when no flag is set */
#define DEFAULT_MUTATION_PERCENTAGE 5

#define DELTA_ACCEPT_AS_SETTLING 0.00001
#define SETTLED_ACCEPT_AS_FINISHED 5

#define DEBUG_PROGRAM 0


/** Prints the command header with program name, version etc. */
void header() {
    printf("Genetic Function Optimisation\n");
    printf("Copyright (c) Milan Kladivko, 2018\n");
    printf("\n");
}


/** Prints the command help. */
void help() {
    printf("Usage: \n   ");
    printf("gfo <metadata_filename> <generations> [-m <mutation_percent>]\n");
    printf("\nOptions: \n   ");
    printf("-m <mutation_percent> ... Sets a custom mutation percentage\n");
    printf("                          (int, 0 to 100)\n");
    printf("\nExample: \n   ");
    printf("...gms ./functions/meta.txt 1000 -m 5\n");
}


/** Handles the command arguments. */
void handle_params(int argc, char *argv[]) {
    
    int i;
    int temp;
    char *ptr;
    char *filename;
    
    header();
    
    /* Not enough arguments */
    if (argc < 3) {
        help();
        exit(EXIT_FAILURE);
    }
    
    
    /* Argument metadata_filename */
    _meta_filename = argv[1];
    
    
    /* Generations arguments */
    _pop->gen_target = strtol(argv[2], &ptr, 10);
    if (!ptr) {
        printf("[ERROR] Generations is not a valid number. \n");
        exit(EXIT_FAILURE);
    }
    if (_pop->gen_target <= 0) {
        printf("[ERROR] Generations have to be positive. \n");
        exit(EXIT_FAILURE);
    }
    
    
    /* Handle option arguments */
    for (i = 2; i < argc; i++) {
        if (argv[i][0] != '-') continue;        /* arg is option */
        if (toupper(argv[i][1]) == 'M') {       /* flag is -m */
            
            temp = strtol(argv[i+1], &ptr, 10); /* get next argument as int */
            
            if (!ptr) {
                printf("[INFO] Mutation percentage not a number, ignoring.\n");
                continue;
            }
            if (temp < 0 || temp > 100) {
                printf("[INFO] Mutation percentage out of bounds, ignoring.\n");
                continue;
            }
            
            printf("[INFO] Setting mutation percentage to %d.\n", temp);
            _pop->mutate_perc = temp;
        }
    }
    
}

/** Ends the program in a safe manner - closes all files and not-freed structs 
    and prints a message about aborting. */
void end() {
    
    int i;
    
    if (_func_cmd != NULL)  {
        free(_func_cmd);
        _func_cmd = NULL;
    }
    
    if (_pop != NULL) { 
        /* destroy_population(&_population); /**/
        if (_pop->members != NULL) {
            for (i = 0; i < _pop->members_n; i++) {
                if ((_pop->members)[i] != NULL) {
                    destroy_member( &((_pop->members)[i]) ); 
                    /* ....what a line! */
                    (_pop->members)[i] = NULL;
                }
            }
            free(_pop->members);
            _pop->members = NULL;
        }
        if (_pop->vars != NULL) {
            for (i = 0; i < _pop->vars_n; i++) {
                if ((_pop->vars)[i] != NULL) {
                    destroy_variable( &((_pop->vars)[i]) );
                }
            }
            free(_pop->vars);
            _pop->vars = NULL;
        }
        free(_pop);
        _pop = NULL;
    }
    
    printf("\nProgram now closing...\n\n");
}

/** Handles the initialization of global variables, adds exit event handler, 
    allocates needed memory etc. */
void prepare() {
    
    FILE * filetest = NULL;
    
    /* set randomization seed using current timestamp */
    srand((unsigned) time(NULL));
    
    /* add exit event handler */
    atexit(&end);
    
    
    /* globals initialization */
    GENE_INT = 'Z';
    GENE_REAL = 'R';
    DEBUG = DEBUG_PROGRAM; 
    
    /* Project files */
    _meta_filename = NULL;
    _func_cmd = NULL;
    
    _gen_filename = GEN_FILENAME;
    filetest = fopen(_gen_filename, "w");
    if (!filetest) {
        printf("[ERROR] Couldn't open generation summary output file:\n");
        printf("        %s", strerror(errno));
        exit(EXIT_FAILURE);
    } else {
        fclose(filetest); filetest = NULL;
    }
    _val_filename = VAL_FILENAME;
    filetest = fopen(_val_filename, "w");
    if (!filetest) {
        printf("[ERROR] Couldn't open value output file:\n");
        printf("        %s", strerror(errno));
        exit(EXIT_FAILURE);
    } else {
        fclose(filetest); filetest = NULL;
    }
    
    /* population global pack */
    _pop = (Population *) malloc(sizeof(Population));
    if (!_pop) {
        printf("[ERROR] Couldn't allocate space for population.");
        exit(EXIT_FAILURE);
    }
    _pop->members = NULL;
    _pop->members_n = -1;
    
    _pop->gen_target = -1;
    _pop->gen_current = -1;
    
    _pop->mutate_perc = DEFAULT_MUTATION_PERCENTAGE;
    
    _pop->vars = NULL;
    _pop->vars_n = -1;
        
}

/** Runs the main program algorithms. Exists to not clutter main(). */
void run() {
    int success;
    double best, previous_best, delta;
    int settled = 0;
    
    printf("[INFO] Running the main program...\n");
    
    success = 
        process_metafile(_meta_filename); 
    if (!success) {
        printf("[ERROR] Initial metadata processing failed.\n");
        exit(EXIT_FAILURE);
    }
    
    
    /* Create first generation */
    _pop->members_n = POPULATION_COUNT;
    
    first_generation();
    update_population_fitness();
    sort_population_by_fittest_first();
    
    best = _pop->members[_pop->members_n - 1]->fitness;
    
    /* Main breeding loop */
    while (_pop->gen_current < _pop->gen_target) { 
        
        next_generation();
        mutate_population();
        
        update_population_fitness();
        sort_population_by_fittest_first();
        
        write_generation_summary();
        
        /* Check if fitness values are settling */
        previous_best = best;
        best = _pop->members[_pop->members_n - 1]->fitness;
        delta = abs(previous_best - best);
        
        if (delta < DELTA_ACCEPT_AS_SETTLING) 
            settled++;
        else settled = 0;
        
        if (settled > SETTLED_ACCEPT_AS_FINISHED) {
            printf("The fitness value settled at %lf (generation %d)\n", 
                best, _pop->gen_current);
            break;
        }
        
        
        _pop->gen_current++;
    }
    
}




/**
    Program arguments (argv): 
     [1] string: 
        Tested function metadata file name
     [2] <uint>: 
        Number of generations
    
    Optional arguments: 
     [3] -m <int (0-100)>: 
        Mutation percentage
*/
int main(int argc, char *argv[]) {
    
    prepare();
    handle_params(argc, argv);
    run();
    /* end(); not needed, runs automatically via atexit() */
    
    return EXIT_SUCCESS;
}


/*
    Format of the metadata file: 
    #_e:\functions\fce.exe
    
    #_(5,90);Z
    X=0
    Z=25
    
    
    Output of the program:
        ...summary of optimum searching
         
        gen.txt
            List of generations
            format: 
                --- GENERATION 598 ---
                58.236
                X=45#(5,90);Z
                Y=0.28#(0.1,1.0);R
                
                --- GENERATION 597 ---
                57.786
                X=40#(5,90);Z
                Y=0.24#(0.1,1.0);R
                .
                .
            
        val.txt
            Values of function and results
            format: 
                58.236
                X=7#(5,90);Z
                Y=0.11#(0.1,1.0);R
                58.236
                X=9#(5,90);Z
                Y=0.15#(0.1,1.0);R
                .
                .
*/
