/*
    
    Function Optimisation Using a Genetic Algorithm
    Seminar work of `Programming in C' (year 2018)
    
    Author: Milan Kladivko
    
    
    Header file  genetics.h:
    For details see module file  genetics.c 
    
    Structures and functions handling the genetic algorithms.  
    
*/

#ifndef __GENETICS_H
#define __GENETICS_H

#include "io.h"

/*  ==========================================================================  
    GENE STRUCT  
*/

/* Forward reference for gene */
typedef struct Variable_ Variable;

/** Gene containing a value, real or integer. */
typedef struct Gene_ {
    Variable * variable;
    double real_val;
    int int_val;
    /* #TODO use Variable * for gene data */
} Gene;

Gene *new_gene(Variable *variable);
void destroy_gene(Gene **gene);
void print_gene(Gene *gene);


/*  ==========================================================================  
    MEMBER STRUCT  
*/

/** A member carrying genes. */
typedef struct Member_ {
    Gene **genes;           /**< Chromosomes */
    int genes_n;            /**< Number of genes in gene-pool */
    double fitness;         /**< Member's fitness to reproduce */
} Member;

Member *new_member(int n_of_genes);
void destroy_member(Member **member);
void print_member(Member * member);


/*  ============================================================================
    MANIPULATION FUNCTIONS
*/


/** Create the first generation of the population */
void first_generation(); 

/** Create a new generation from the previous (existing) one */
void next_generation();

/** Update the whole population's fitness scores */
void update_population_fitness();

/** Sorts population members by their fitness scores */
void sort_population_by_fittest_first();

/** Mutates members based on their fitness and the mutation_perc global value 
    Returns number of mutated members */
void mutate_population();


#endif
