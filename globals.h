/*
    
    Function Optimisation Using a Genetic Algorithm
    Seminar work of `Programming in C' (year 2018)
    
    Author: Milan Kladivko
    
    
    Header file  `globals.h': 
    
    File contains definitions of global variables. These variables include
    command line arguments and variables that are directly created
    using these arguments. 
    Debug flags and/or error flags can also be seen in this file.    
    
*/


#ifndef _GLOBALS_H
#define _GLOBALS_H

#include <stdio.h>

#include "genetics.h"
#include "io.h"

char *_meta_filename;   /**< Function meta file */
char *_func_cmd;        /**< Function executable (for fitness) */
char *_val_filename;
char *_gen_filename;


typedef struct Population_ 
{
    /** Currently living members */
    Member ** members;
    /** Target number of members in population */
    int members_n;
    
    /** A wanted number of generations */
    int gen_target;
    /** Current generation of population */
    int gen_current;
    
    /** Mutation percentage */
    int mutate_perc;
    
    /** Variables that are described by members' genes */
    Variable ** vars;
    /** Number of defined variables in the list */
    int vars_n;
    
} Population;

Population * _pop;

char GENE_INT; 
char GENE_REAL;

#define YES 1
#define NO  0
int DEBUG;     /**< Runtime debug output flag */


/* Macro for getting the number of elements in list */ 
#define ARR_LENGTH(arr)  (sizeof(arr) / sizeof((arr)[0]))


#endif
