/*
    
    Function Optimisation Using a Genetic Algorithm
    Seminar work of `Programming in C' (year 2018)
    
    Author: Milan Kladivko
    
    
    Module file  `io.c': 
    
    File contains functions taking care of reading and writing 
    all files required in this program. 
    
*/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "globals.h"
#include "io.h"
#include "genetics.h"

/* Local constants */
static const int MAX_LINE_LEN = 256;
static const int MAX_VARNAME_LEN = 32;
static const char meta_params_format[] = "#_(%[^,],%[^)]);%c";
static const char meta_varname_format[] = "%[^=]=";
static const char meta_exec_format[] = "#_%s";  /* #TODO better checking? */
static const char temp_filename[] = "metatemp.txt";
static const char format_varint[] =  "%s=%d#(%d,%d);%c\n";
static const char format_varreal[] = "%s=%.4lf#(%5.1lf,%5.1lf);%c\n";

/* Local functions */
static char * parse_executable(FILE *metafile);
static int parse_vars(FILE *metafile, Variable ** vars); 
static int modify_metafile(Member *);


/*  ========================================================================  */


/** 
    Processes the metafile and saves the parameters found in the file. 
    The parameters include: 
    - the path to the function executable (first line)
    - changeable variables later used in gene pool of members (and their params)
    For details see 'Format souboru s metadaty' in assignment. 
*/
int process_metafile(char *meta) {
    
    unsigned line_number = 0;
    FILE * metafile = NULL;
    
    metafile = fopen(meta, "r");
    if (!metafile) {
        printf("[ERROR] Couldn't open function metadata file.\n");
        return 0;
    }
    
    /* Parse the executable file name from first line */
    _func_cmd = parse_executable(metafile);
    if (_func_cmd == NULL) return 0;
    
    if (DEBUG) printf("...executable @ %s\n", _func_cmd);
    
    /* Count parse-able variables only (thus the NULL) */
    _pop->vars_n = parse_vars(metafile, NULL);
    
    /* Get variables and their metadata */
    _pop->vars = 
        (Variable **) malloc(_pop->vars_n * sizeof(Variable*));
    if (_pop->vars == NULL) {
        printf("[ERROR] Could not allocate memory for variables.");
        free(_func_cmd);
        _func_cmd = NULL;
        return 0;
    }
    
    /* Finally fill variables with data from metafile */
    parse_vars(metafile, _pop->vars);
    
    fclose(metafile);
    metafile = NULL;
    return 1;
}

static int parse_vars(
    FILE *metafile, Variable ** vars) 
{
    char line[MAX_LINE_LEN];
    char line2[MAX_LINE_LEN];
    
    char min[32];
    char max[32];
    char type;
    char varname[MAX_VARNAME_LEN];
    int line_number = 0;
    
    int filled;
    int parsed = 0;
    
    /* Read file from the start */
    fseek(metafile, 0, SEEK_SET);
    
    while (fgets(line, MAX_LINE_LEN, metafile) != NULL) {
        line_number++;
        
        if (DEBUG) 
            printf("%3d | %s", line_number, line);
        
        filled = sscanf(
            line, meta_params_format, 
            min, max, &type);
        if (filled != 3) continue;
        type = toupper(type);
        
        if (fgets(line2, MAX_LINE_LEN, metafile) != NULL) {
            line_number++;
            
            if (DEBUG) 
                printf("%3d | %s", line_number, line2);
            
            filled = sscanf(line2, meta_varname_format, varname);
            if (filled != 1) continue;
            
            if (vars != NULL) { 
                vars[parsed] = new_variable(
                    line_number, min, max, type, varname);
            }
            
            parsed++;
        }
    }
    return parsed;
}

static char * parse_executable(FILE *metafile) {
    
    char line[MAX_LINE_LEN];
    char *exec_filename = NULL;
    
    exec_filename = (char *) malloc(MAX_LINE_LEN);
    if (exec_filename == NULL) {
        printf("[ERROR] Memory allocation error while parsing function.\n");
        return NULL;
    }
    
    /* Read file from the start */
    fseek(metafile, 0, SEEK_SET);
    
    /* Read first line of metadata file for function file  */
    if (fgets(line, MAX_LINE_LEN, metafile) == NULL) {
        printf("[ERROR] Couldn't read from metadata file.\n");
        return NULL;
    }
    if (sscanf(line, meta_exec_format, exec_filename) < 1) {
        printf("[ERROR] First line of metadata file is not valid:\n");
        printf("        \"%s\"", line);
        return NULL;
    }
    
    /* Check if function file exists */
    if (access(exec_filename, F_OK) == -1) {
        printf("[ERROR] The passed function file doesn't exist:\n");
        printf("        \"%s\".\n", exec_filename);
        return NULL;
    }
    
    /* Check if function is executable */
    if (access(exec_filename, X_OK) == -1) {
        printf("[ERROR] The passed function file can't be run:\n");
        printf("        \"%s\".\n", exec_filename);
        return NULL;
    }
    
    return exec_filename;
}


/*  ========================================================================  */


Variable * new_variable(
    unsigned line, 
    char *min, char *max, char type, 
    char *name) 
{
    
    Variable * new = NULL;
    char * varname = NULL;
    double min_d, max_d;
    
    if (min == NULL || max == NULL || name == NULL) {
        printf("[ERROR] Null parameters in new variable.\n");
        return NULL;
    }
    if (type != GENE_INT && type != GENE_REAL) {
        printf("[ERROR] Invalid variable type: %c", type);
        return NULL;
    }
    
    /* Get min and max parameters */
    if (sscanf(min, "%lf", &min_d) != 1) {
        printf("[ERROR] Couldn't parse min parameter of var.\n");
        return NULL;
    }
    if (sscanf(max, "%lf", &max_d) != 1) {
        printf("[ERROR] Couldn't parse max parameter of var.\n");
        return NULL;
    }
    
    /* Fill new variable */ 
    new = (Variable *) malloc(sizeof(Variable));
    if (new == NULL) {
        printf("[ERROR] Couldn't allocate memory for variable.\n");
        return NULL;
    }
    new->line = line;
    new->min = min_d;
    new->max = max_d;
    new->type = type;
    
    /* Copy variable name string into struct */
    varname = (char *) malloc(MAX_VARNAME_LEN);
    if (varname == NULL) {
        printf("[ERROR] Couldn't allocate memory for varname.\n");
        free(new);
        return NULL;
    }
    strncpy(varname, name, MAX_VARNAME_LEN);
    new->name = varname;
    
    return new;
}

void destroy_variable(Variable ** var) {
    if ((*var)->name != NULL) free((*var)->name);
    free(*var);
}

char * tostr_variable(Variable * var) {
    char *buffer = (char *) malloc(64);
    sprintf(buffer, "%-3d | (%.2lf,%.2lf);%c | %s", 
        var->line, var->min, var->max, var->type, var->name);
    return buffer;
}


/*  ========================================================================  */


/** 
    Changes the meta file to genes of this member and evaluates the member's
    fitness which is then returned.  
*/
double fitness_of(Member *member) {
    
    const double ERR_VALUE = -INFINITY;
    int success;
    
    if (member == NULL) {
        printf("[ERROR] Cannot get fitness of a null member\n");
        return ERR_VALUE;
    }
    
    
    success = modify_metafile(member);
    if (!success) return ERR_VALUE;
    
    
    double fitness;
    char line[MAX_LINE_LEN];
    FILE *exec = NULL;
    
    exec = popen(_func_cmd, "r");
    if (exec == NULL) {
        printf("[ERROR] Couldn't execute fitness function.\n");
        return ERR_VALUE;
    }
    
    while (fgets(line, MAX_LINE_LEN, exec) != NULL) {
        
        // if (DEBUG) printf(">> %s", line);
        
        success = sscanf(line, "%lf", &fitness);
        if (success == 1) {
            pclose(exec); exec = NULL;
            return fitness;
        }
    }
    
    printf("[ERROR] Couldn't decode fitness function's output\n");
    pclose(exec); exec = NULL;
    return -INFINITY;
}


static int modify_metafile(Member * member) {
    
    char line[MAX_LINE_LEN];
    unsigned line_n;
    
    int gene_index = 0;
    Gene * gene = NULL;
    unsigned next_stop = 0;
    
    FILE *meta = fopen(_meta_filename, "r");
    if (!meta) {
        printf("[ERROR] Couldn't open meta file for reading.\n");
        return 0;
    }
    
    FILE *temp = fopen(temp_filename, "w");
    if (!temp) {
        printf("[ERROR] Couldn't open a temp file for writing.\n");
        return 0;
    }
    
    line_n = 0;
    gene_index = 0;
    next_stop = member->genes[0]->variable->line;
    while (fgets(line, MAX_LINE_LEN, meta) != NULL) 
    {
        line_n++;
        
        if (next_stop != line_n)
            fprintf(temp, "%s", line);
            
        else {
            gene = member->genes[gene_index++];
            
            /* Print a modified line to temp */
            if (gene->variable->type == GENE_INT) 
                fprintf(temp, "%s= %d\n", 
                    gene->variable->name, gene->int_val);
            else if (gene->variable->type == GENE_REAL)
                fprintf(temp, "%s= %lf\n", 
                    gene->variable->name, gene->real_val);
                    
            /* Find next variable in file, if possible */
            if (gene_index < member->genes_n) 
                next_stop = 
                    member->genes[gene_index]->variable->line;
        }
    }
    
    fclose(temp); temp = NULL;
    fclose(meta); meta = NULL;
    
    remove(_meta_filename);
    rename(temp_filename, _meta_filename);
    
    return 1;
}


void write_generation_summary() {
    
    FILE * gen = NULL;
    
    Member * best = NULL;
    Gene * gene = NULL;
    char * name;
    double min, max;
    char type;
    int i;
    
    /* #TODO write summary file entries */
    if (DEBUG) 
        printf("Writing summary of generations.\n");
    
    gen = fopen(_gen_filename, "a");
    if (!gen) {
        printf("[INFO] Couldn't append to the generation summary file\n");
        return;
    }
    
    best = _pop->members[0];
    fprintf(gen, "--- GENERATION %d ---\n", _pop->gen_current);
    fprintf(gen, "%lf\n", best->fitness);
    for (i = 0; i < best->genes_n; i++) {
        gene = best->genes[i];
        
        name = gene->variable->name;
        min = gene->variable->min;
        max = gene->variable->max;
        type = gene->variable->type;
        
        if (type == GENE_INT) {
            fprintf(gen, format_varint, 
                name, gene->int_val, (int)min, (int)max, type);
        } else if (type == GENE_REAL) {
            fprintf(gen, format_varreal, 
                name, gene->real_val, min, max, type);
        }
    }
    fprintf(gen, "\n");
    
    
    fclose(gen); gen = NULL;
}

void write_member_summary(Member * m) {
    
    FILE * val = NULL;
    int i;
    
    Gene * gene = NULL;
    char * name;
    double min, max;
    char type;
    
    if (!m) {
        printf("[INFO] Cannot write a summary of a null member.\n");
        return;
    }
    
    val = fopen(_val_filename, "a");
    if (!val) {
        printf("[INFO] Couldn't append text to %s.\n", _val_filename);
        return;
    }
    
    fprintf(val, "%lf\n", m->fitness);
    for (i = 0; i < m->genes_n; i++) {
        gene = m->genes[i];
        
        name = gene->variable->name;
        min = gene->variable->min;
        max = gene->variable->max;
        type = gene->variable->type;
        
        if (type == GENE_INT) {
            fprintf(val, format_varint, 
                name, gene->int_val, (int)min, (int)max, type);
        } else if (type == GENE_REAL) {
            fprintf(val, format_varreal, 
                name, gene->real_val, min, max, type);
        }
    }
    fprintf(val, "\n");
    
    fclose(val); val = NULL;
}
