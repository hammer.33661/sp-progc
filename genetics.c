/*
    
    Function Optimisation Using a Genetic Algorithm
    Seminar work of `Programming in C' (year 2018)
    
    Author: Milan Kladivko
    
    
    Module file  `genetics.c': 
    
    File contains functions and structures related to genetic
    algorithms used in the program. 
    
*/

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "genetics.h"
#include "globals.h"
#include "io.h"


static Gene * cross(Gene *, Gene *);
static Gene * mutate(Gene *);
static Member * breed(Member *, Member *);

/*  ========================================================================  */
/*  ========================================================================  */


Gene *new_gene(Variable * variable) {
    Gene * this = NULL;
    
    if (variable == NULL) {
        printf("[ERROR] Cannot create gene from null variable.\n");
        return NULL;
    }
    
    this = (Gene *) malloc(sizeof(Gene));
    if (this == NULL) {
        printf("[ERROR] Could not allocate memory for new gene.\n");
        return NULL;
    }
    
    this->variable = variable;
    return this;
}

void destroy_gene(Gene **gene) {
    if (!gene || !(*gene)) return;
    free(*gene); *gene = NULL;
}

void print_gene(Gene *gene) {
    if (gene->variable->type == GENE_INT) {
        printf("(%.2f,%.2f);%c = %d   ", 
            gene->variable->min, 
            gene->variable->max, 
            gene->variable->type, 
            gene->int_val);
    } else if (gene->variable->type = GENE_REAL) {
        printf("(%.2f,%.2f);%c = %lf   ", 
            gene->variable->min, 
            gene->variable->max, 
            gene->variable->type, 
            gene->real_val);
    }
    
}


/*  ========================================================================  */


Member *new_member(int n_of_genes) {
    Member * this = NULL;
    
    this = (Member *) malloc(sizeof(Member));
    if (this == NULL) {
        printf("[ERROR] Could not allocate memory for new member");
        return NULL;
    }
    
    this->fitness = -INFINITY;
    this->genes_n = n_of_genes;
    this->genes = (Gene**) malloc(n_of_genes * sizeof(Gene*));
    return this;
}

void destroy_member(Member **member) {
    int i;
    Member *m;
    
    if (member == NULL) return;
    if (*member == NULL) return;
    m = *member;
    if (m->genes != NULL) {
        for (i = 0; i < m->genes_n; i++) 
            destroy_gene(&(m->genes[i]));
        free(m->genes);
    }
    free(*member); *member = NULL;
}

void print_member(Member * member) {
    int i;
    
    printf("\nFitness: %.4f\n", member->fitness);
    for (i = 0; i < member->genes_n; i++) {
        printf("  ");
        print_gene(member->genes[i]);
        printf("\n");
    }
}


/*  ========================================================================  */
/*  ========================================================================  */


void first_generation() {
    int i, j;
    double roll;
    
    Member * member = NULL;
    Gene * gene = NULL;
    Variable * variable = NULL;
    
    if (DEBUG) printf("\nCreating the first generation...\n");
    
    _pop->members = 
        (Member **) malloc(_pop->members_n * sizeof(Member *));
    _pop->gen_current = 1;
    
    for (i = 0; i < _pop->members_n; i++) 
    {
        member = new_member(_pop->vars_n);
        for (j = 0; j < _pop->vars_n; j++) 
        {
            variable = _pop->vars[j];
            gene = new_gene(variable);
            
            roll = (double) rand() / RAND_MAX;
            
            if (variable->type == GENE_REAL) 
            {
                /* Initial value = random number in (min,max) interval */
                gene->real_val = (double) (
                    variable->min + roll * (variable->max - variable->min));
            } 
            else if (variable->type == GENE_INT) 
            {
                /* Same here */
                gene->int_val = (int) round(
                    variable->min + roll * (variable->max - variable->min));
            }
            
            member->genes[j] = gene;
        }
        _pop->members[i] = member;
        
        // if (DEBUG) print_member(member); 
    }
}


/*  ========================================================================  */


void next_generation() {
    
    const double FITNESS_BIAS = 0.6; 
    int i, j, n = _pop->members_n;
    double roll; 
    Member ** new_generation = NULL;
    Member *mem_a = NULL, *mem_b = NULL, *new = NULL;
    
    printf("\nCreating next generation...\n");
    
    new_generation = (Member **) malloc(n * sizeof(Member*));
    if (!new_generation) {
        printf("[ERROR] Couldn't allocate memory for a new generation.\n");
        return;
    }
    
    i = 0;
    while (i < n) {
        
        do {
            roll = pow((double) rand() / RAND_MAX, 1/(1-FITNESS_BIAS));
            j = (int) (roll * n);
            mem_a = _pop->members[j];
        } while (mem_a == NULL);
        
        do {
            roll = pow((double) rand() / RAND_MAX, 1/(1-FITNESS_BIAS));
            j = (int) (roll * n);
            mem_b = _pop->members[j];
        } while (mem_b == NULL || mem_b == mem_a);
        
        new = breed(mem_a, mem_b);
        if (!new) {
            printf("[INFO] Couldn't breed two members.\n");
        }
        
        new_generation[i++] = new;
        
        if (DEBUG) printf(" .");
    }
    
    if (DEBUG) printf("\n Created a new generation.\n");
    
    /* Dispose of the previous generation */
    for (i = 0; i < n; i++) destroy_member(&(_pop->members[i]));
    free(_pop->members);
    _pop->members = new_generation;
}


/*  ========================================================================  */


void update_population_fitness() {
    
    int i, n = _pop->members_n;
    double fitness;
    Member * mem;
     
    printf("\nUpdating the population's fitnesss scores...\n");
    
    for (i = 0; i < n; i++) {
        mem = _pop->members[i];
        if (!mem) 
            printf("[INFO] Cannot update a null member's fitness (%d)\n", i);
        
        mem->fitness = fitness_of(mem);
        
        write_member_summary(mem);
        
        if (mem->fitness == -INFINITY) {
            printf("[INFO] Couldn't calculate fitness of a member.\n");
        }
    }
}


/*  ========================================================================  */


void sort_population_by_fittest_first() {
    
    /* Using selection sort */
    
    int i, j, n, fittest_index = -1;
    Member *member = NULL, *fittest = NULL, *temp = NULL;
    
    printf("\nSorting the population by fitness...\n");
    
    n = _pop->members_n;
    for (i = 0; i < n; i++) 
    {
        fittest_index = i;
        fittest = _pop->members[i];
        for (j = i+1; j < n; j++) 
        {
            member = _pop->members[j];
            if (member->fitness > fittest->fitness) 
            {
                fittest = member;
                fittest_index = j;
            }
        }
        
        temp = _pop->members[i];
        _pop->members[i] = fittest;
        _pop->members[fittest_index] = temp;
    }
    
    if (DEBUG) {
        for (i = 0; i < n; i++) { 
            // printf(" | %.3lf", _pop->members[i]->fitness);
        }
    }
    
    printf("Best score in current generation: %lf\n", 
        _pop->members[n-1]->fitness);
}


/*  ========================================================================  */


void mutate_population() {
    
    int i, j, n;
    double roll;
    Member *m = NULL;
    double chance; 
    double average_chance = _pop->mutate_perc / 100.0;
    int mutated = 0;
    
    printf("\nMutating the population (chance=%.2lf%%).\n", 
        average_chance * 100);
    
    if (_pop->members == NULL) {
        printf("[ERROR] Cannot mutate a null list of members.\n");
        return;
    }
    
    n = _pop->members_n;
    for (i = 0; i < n; i++) {
        
        /* Using (i/n)^(1/c-1) */
        // chance = pow((double) i / n, 1 / average_chance - 1);
        chance = average_chance;
        roll = (double) rand() / RAND_MAX;
        
        /* Mutate away the unfit more often than the fit ones */
        if (roll < chance) { 
            m = _pop->members[i];
            if (m == NULL) { 
                printf("[INFO] Cannot mutate null member.\n");
                continue;
            }
            
            for (j = 0; j < m->genes_n; j++) 
                m->genes[j] = mutate(m->genes[j]);
            mutated++;
            
            if (DEBUG) printf(" [%d] mutated\n", i);
        }
    }
    if (DEBUG) printf("\n");;
}


/*  ========================================================================  */
/*  ========================================================================  */


/** Crosses the two genes and returns a new gene. */
static Gene * cross(Gene *a, Gene *b) {
    
    Gene * crossed = NULL;
    unsigned halfmask;
    int split;
    
    if (a == NULL || b == NULL) {
        printf("[ERROR] Crossing null genes!");
        return NULL;
    }
    if (a->variable != b->variable) {
        printf("[ERROR] Crossing genes for different variables!");
        return NULL;
    }
    
    /* #TODO implement boundaries for genes */
    crossed = new_gene(a->variable);
    if (crossed == NULL) {
        printf("[ERROR] Couldn't create gene for crossing.");
        return NULL;
    }
    
    if (a->variable->type == GENE_REAL) 
    {
        /* Arithmetic average of both real values */
        crossed->real_val = (a->real_val + b->real_val) / 2; 
    } 
    else if (a->variable->type == GENE_INT) 
    {
        /* Combine bits of both, randomize where to split */
        split = (int)round( 
            ((double) rand() / RAND_MAX) * (sizeof(int)*8) );
        halfmask = (1U << split) - 1;
        crossed->int_val = 
            (a->int_val & halfmask) + (b->int_val & ~halfmask);
    }
}

/** Randomly mutates the gene */
static Gene * mutate(Gene *gene) {
    // #TODO mutate genes according to the assignment
}

static Member * breed(Member *a, Member *b) {
    
    Member *new = NULL;
    Gene *crossed = NULL;
    int i;
    int n_of_genes;
    
    if (a == NULL || b == NULL) { 
        printf("[ERROR] Cannot breed NULL members.");
        return NULL; 
    }
    if (a->genes_n != b->genes_n) {
        printf("[ERROR] Can't breed when unmatched gene-pools.");
        return NULL;
    }
    
    new = new_member(a->genes_n);
    if (new == NULL) return NULL;
    for (i = 0; i < a->genes_n; i++) {
        
        crossed = cross(a->genes[i], b->genes[i]);
        if (crossed == NULL) return NULL;
        
        new->genes[i] = crossed;
    }
    return new;
}
